--
-- journal of all changes to all audited tables
-- set search_path to _pg_dml_audit_model;
--


DROP SCHEMA IF EXISTS tablewatch CASCADE;
CREATE SCHEMA tablewatch;
COMMENT ON SCHEMA tablewatch IS '
  Out-of-table audit/history logging
  Audit application schema.
  Basic concept is taken from http://www.postgresql.org/docs/9.4/static/functions-info.html';
REVOKE ALL ON SCHEMA tablewatch FROM PUBLIC;
SET SEARCH_PATH TO tablewatch, public;

CREATE TYPE trackevents AS ENUM ('INSERT', 'UPDATE', 'DELETE', 'TRUNCATE', 'SNAPSHOT' );

COMMENT ON TYPE trackevents IS $$
The type of rows in the event table extends the possible values of TG_OPs
with a SNAPSHOT operation type to allow easy reconstruction table states
- INSERT   the new row is in rowdata
- UPDATE   both rows go in rowdata OLD first, NEW second -- TODO: add json attribute for NEW/OLD
- DELETE   the old row is in rowdata
- TRUCATE  save all rows into rowdata
- SNAPSHOT marks start and end of auditing a table
$$;


CREATE TABLE IF NOT EXISTS events (
  nspname   TEXT        NOT NULL,
  relname   TEXT        NOT NULL,
  usename   TEXT        NOT NULL,
  trans_ts  TIMESTAMPTZ NOT NULL,
  trans_id  BIGINT      NOT NULL,
  trans_sq  INTEGER     NOT NULL,
  eventype  trackevents NOT NULL,
  rowdata   JSONB,
  CONSTRAINT events_pkey PRIMARY KEY (trans_ts, trans_id, trans_sq) -- TODO: find optimal order
);

COMMENT ON TABLE  events IS 'History of auditable actions on audited tables, from audit.if_modified_func()';
COMMENT ON COLUMN events.nspname  IS 'database schema name of the audited table';
COMMENT ON COLUMN events.relname  IS 'name of the table changed by this event';
COMMENT ON COLUMN events.usename  IS 'Session user whose statement caused the audited event'; - TODO: is this what we need?
COMMENT ON COLUMN events.trans_ts IS 'Transaction timestamp for tx in which audited event occurred (PK)';
COMMENT ON COLUMN events.trans_id IS 'Identifier of transaction that made the change. (PK)';
COMMENT ON COLUMN events.trans_sq IS 'make multi-row-transactions unique. (PK)';
COMMENT ON COLUMN events.eventype IS 'event operation of type audit.op_types';
COMMENT ON COLUMN events.rowdata  IS 'Old and new rows affected by this event';

-- ideas for next iteration:
-- COMMENT ON COLUMN audit.events.nodata   IS '''t'' if audit event is from an FOR EACH STATEMENT trigger, ''f'' for FOR EACH ROW';
-- COMMENT ON COLUMN audit.events.query    IS 'Top-level query that caused this auditable event. May be more than one statement.';
-- COMMENT ON COLUMN audit.events.appname  IS 'postgres ''application_name'' set when this audit event occurred. Can be changed in-session by client.';
-- COMMENT ON COLUMN audit.events.stmnt_ts IS 'Statement start timestamp for tx in which audited event occurred';
-- COMMENT ON COLUMN audit.events.clock_ts IS 'Wall clock time at which audited event''s trigger call occurred';

REVOKE ALL ON events FROM PUBLIC;




