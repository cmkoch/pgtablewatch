--
-- report on known tables
-- set search_path to _pg_dml_audit_api, pg_catalog;
--

SET SEARCH_PATH TO tablewatch,public;

--
-- central trigger function
--

CREATE OR REPLACE FUNCTION if_modified_func()
  RETURNS TRIGGER AS $body$
DECLARE
  audit_row  events;
  query_text TEXT;
  all_rows   JSON [] := '{}';
  anyrow     RECORD;
BEGIN
  IF TG_WHEN <> 'AFTER' AND TG_OP != 'TRUNCATE'
  THEN
    RAISE EXCEPTION 'if_modified_func() may only run as an AFTER trigger';
  END IF;

  audit_row.nspname = TG_TABLE_SCHEMA :: TEXT;
  audit_row.relname = TG_TABLE_NAME :: TEXT;
  audit_row.usename = SESSION_USER :: TEXT;
  audit_row.trans_ts = transaction_timestamp();
  audit_row.trans_id = txid_current();
  audit_row.trans_sq = 1;
  audit_row.eventype = TG_OP;
  audit_row.rowdata = NULL;

  IF (TG_OP = 'UPDATE')
  THEN
    audit_row.rowdata = array_to_json(ARRAY [row_to_json(OLD), row_to_json(NEW)]); -- save both rows
  ELSIF (TG_OP = 'DELETE')
    THEN
      audit_row.rowdata = array_to_json(ARRAY [row_to_json(OLD)]); -- save old row
  ELSIF (TG_OP = 'INSERT')
    THEN
      audit_row.rowdata = array_to_json(ARRAY [row_to_json(NEW)]); -- save new row
  ELSIF (TG_OP = 'TRUNCATE')
    THEN -- save all rows
      query_text = 'SELECT *  FROM ' || quote_ident(TG_TABLE_SCHEMA) || '.' || quote_ident(TG_TABLE_NAME);
      FOR anyrow IN EXECUTE query_text LOOP
        all_rows = all_rows || row_to_json(anyrow);
      END LOOP;
      audit_row.rowdata = array_to_json(all_rows);
  ELSE
    RAISE EXCEPTION '[if_modified_func] - Trigger func added as trigger for unhandled case: %, %', TG_OP, TG_LEVEL;
    RETURN NULL;
  END IF;

  -- multiple events in the same transaction must be ordered
  LOOP
    BEGIN
      INSERT INTO events (
        nspname, relname, usename, trans_ts, trans_id, trans_sq, eventype, rowdata)
      VALUES (
        audit_row.nspname, audit_row.relname, audit_row.usename, audit_row.trans_ts, audit_row.trans_id,
        audit_row.trans_sq, audit_row.eventype, audit_row.rowdata);
      EXIT; -- successful insert
      EXCEPTION WHEN unique_violation
      THEN
        -- add and loop to try the UPDATE again
        audit_row.trans_sq :=  audit_row.trans_sq + 1;
    END;
  END LOOP;
  RETURN NULL;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;


COMMENT ON FUNCTION if_modified_func() IS $body$
Track changes to a table at the row level.
Note that the user name logged is the login role for the session. The audit trigger
cannot obtain the active role because it is reset by the SECURITY DEFINER invocation
of the audit trigger itself.
$body$;


--
-- save full tabel as json-array
-- set search_path to _pg_dml_audit_api;
--


CREATE OR REPLACE FUNCTION take_snapshot(tableident REGCLASS)
  RETURNS VOID AS $body$
DECLARE
  all_rows   JSON [] := '{}';
  anyrow     RECORD;
  audit_row  events;
  query_text TEXT;

BEGIN
  audit_row.nspname   := (SELECT nspname
                          FROM pg_namespace
                          WHERE OID = (SELECT relnamespace
                                       FROM pg_class
                                       WHERE OID = tableident));
  audit_row.relname   := (SELECT relname
                          FROM pg_class
                          WHERE OID = tableident);
  audit_row.usename = session_user;
  audit_row.trans_ts = transaction_timestamp();
  audit_row.trans_id = txid_current();
  audit_row.trans_sq = 1;
  audit_row.eventype = 'SNAPSHOT';
  audit_row.rowdata = NULL;

  query_text = 'SELECT *  FROM ' || quote_ident(audit_row.nspname) || '.' || quote_ident(audit_row.relname);
  FOR anyrow IN EXECUTE query_text LOOP
    all_rows = all_rows || row_to_json(anyrow);
  END LOOP;
  audit_row.rowdata = array_to_json(all_rows);
  RAISE DEBUG 'take a snapshot';
  INSERT INTO events VALUES
    (audit_row.nspname, audit_row.relname, audit_row.usename, audit_row.trans_ts, audit_row.trans_id,
     audit_row.trans_sq, audit_row.eventype, audit_row.rowdata);
END;
$body$
LANGUAGE 'plpgsql';

COMMENT ON FUNCTION take_snapshot(REGCLASS) IS $body$
Add one row to the audit events as eventype SNAPSHOT saving all rows of the given table into rowdata.
Arguments:
   target_table:     Table name. must be schema qualified if not in  search_path;
Limitations:
   Maximum field size is 1TiByte.
$body$;




--
-- save full tabel as json-array
-- set search_path to _pg_dml_audit_api;
--


CREATE OR REPLACE FUNCTION watch_table(tableident REGCLASS)
  RETURNS VOID AS $body$
DECLARE
  query_text TEXT;

BEGIN
  EXECUTE 'DROP TRIGGER IF EXISTS tablewatch_trigger_row ON ' || tableident;
  EXECUTE 'DROP TRIGGER IF EXISTS tablewatch_trigger_stm ON ' || tableident;

  query_text = 'CREATE TRIGGER tablewatch_trigger_row AFTER INSERT OR UPDATE OR DELETE ON ' || tableident ||
               ' FOR EACH ROW EXECUTE PROCEDURE tablewatch.if_modified_func();';
  RAISE DEBUG '%', query_text;
  EXECUTE query_text;

  query_text = 'CREATE TRIGGER tablewatch_trigger_stm BEFORE TRUNCATE ON ' || tableident ||
               ' FOR EACH STATEMENT EXECUTE PROCEDURE tablewatch.if_modified_func();';
  RAISE DEBUG '%', query_text;
  EXECUTE query_text;

  PERFORM take_snapshot(tableident);

END;
$body$
LANGUAGE 'plpgsql';

COMMENT ON FUNCTION watch_table(REGCLASS) IS $body$
Add auditing triggers to a table and take a initial snapshot.

Arguments:
   target_table:     Table name, schema qualified if not on search_path
$body$;



--
-- ignore_table removes the audit-triggers from a table
-- recorded events remain in events-table
-- set search_path to _pg_dml_audit_api;
a
--

CREATE OR REPLACE FUNCTION ignore_table(tableident REGCLASS)
  RETURNS VOID AS $body$
DECLARE
BEGIN
  EXECUTE 'DROP TRIGGER IF EXISTS tablewatch_trigger_row ON ' || tableident;
  EXECUTE 'DROP TRIGGER IF EXISTS tablewatch_trigger_stm ON ' || tableident;
END;
$body$
LANGUAGE 'plpgsql';

COMMENT ON FUNCTION ignore_table(REGCLASS) IS $body$
Remove followinging triggers from a table. Recorded events remain in the database.
The function has no effect on tables without tablewatch_triggers

Arguments:
   target_table:     Table name, schema qualified if not on search_path
$body$;





CREATE OR REPLACE FUNCTION active_tables()
  RETURNS TABLE(tableident TEXT, audit_active BOOL, evcount INTEGER, firstev TEXT, lastev TEXT) AS $body$

WITH on_record AS (
  SELECT
    nspname || '.' || relname                     AS tableident,
    count(nullif(trans_sq != 1, TRUE)) :: INTEGER AS evcount,
    to_char(min(trans_ts), 'yyyy-MM-dd HH:mm')    AS firstev,
    to_char(max(trans_ts), 'yyyy-MM-dd HH:mm')    AS lastev
  FROM events
  GROUP BY tableident
), on_trigger AS ( SELECT DISTINCT
                     nspname || '.' || relname AS tableident,
                     TRUE                      AS audit_active
                   FROM pg_trigger
                     JOIN pg_class ON tgrelid = pg_class.oid
                     JOIN pg_namespace ON relnamespace = pg_namespace.oid
                   WHERE tgname LIKE 'audit_trigger_%'
)
SELECT
  tableident,
  coalesce(audit_active, FALSE),
  COALESCE(evcount, 0),
  COALESCE(firstev, ''),
  COALESCE(lastev, '')
FROM on_record
  FULL JOIN on_trigger USING (tableident);


$body$
LANGUAGE 'sql';

COMMENT ON FUNCTION active_tables() IS $$
list tables with audit status and some stats from data gathered by audit triggers.
$$;


--
-- return selected rows from events-table as tabel or json object
-- SET search_path TO _pg_dml_audit_api, pg_catalog;
--

CREATE OR REPLACE FUNCTION report_events(tableident TEXT DEFAULT NULL, timeframe DATERANGE DEFAULT NULL)
  RETURNS SETOF events AS $body$
DECLARE
  query_text TEXT;
  event_row  events;
  l_nspname  TEXT;
  l_relname  TEXT;
  retrow     RECORD;
BEGIN
  query_text := 'SELECT * FROM events WHERE TRUE ';

  IF timeframe IS NOT NULL
  THEN
    query_text := query_text || ' AND trans_ts::date <@  ' || quote_literal(timeframe) || '::daterange ';
  END IF;

  IF tableident IS NOT NULL
  THEN
    IF strpos(tableident, '.') > 0
    THEN
      l_nspname := split_part(tableident, '.', 1);
      l_relname := split_part(tableident, '.', 2);
    ELSE
      l_nspname := 'public';
      l_relname := tableident;
    END IF;
    query_text :=query_text || 'AND events.nspname = ' || quote_literal(l_nspname) ||
                 ' AND events.relname =  ' || quote_literal(l_relname);
  END IF;

  RETURN QUERY EXECUTE query_text || ' order by trans_ts desc,  trans_id asc,  trans_sq asc';

END;
$body$
LANGUAGE 'plpgsql';

COMMENT ON FUNCTION report_events(TEXT, DATERANGE) IS $$
Return (filtered) audit-trail as table.

Arguments:
   tablename:     text with schema if ambiguos
   timeframe:     limit query to given range of dates
$$;


CREATE OR REPLACE FUNCTION report_events(timeframe DATERANGE, tableident TEXT DEFAULT NULL)
  RETURNS SETOF events AS $body$

SELECT *
FROM report_events(tableident, timeframe)

$body$
LANGUAGE 'sql';

COMMENT ON FUNCTION report_events(DATERANGE, TEXT) IS $$
Reverse arguments wrapper for report_events(TEXT, DATERANGE)
$$;


--
-- select events based on recorded changes
--  set search_path to _pg_dml_audit_api, pg_catalog;
--

CREATE OR REPLACE FUNCTION report_attribute(kvlist    TEXT, tableident TEXT DEFAULT NULL,
                                            timeframe DATERANGE DEFAULT NULL)
  RETURNS SETOF events AS $body$
DECLARE
  query_text TEXT;
  event_row  events;
  l_nspname  TEXT;
  l_relname  TEXT;
BEGIN

  query_text := 'WITH subq AS (
                   SELECT nspname, relname, usename, trans_ts, trans_id, trans_sq, eventype,
                          jsonb_array_elements(rowdata) AS singlerow
                   FROM events WHERE TRUE ';

  IF timeframe IS NOT NULL
  THEN
    query_text := query_text || ' AND trans_ts::date <@  ' || quote_literal(timeframe) || '::daterange ';
  END IF;

  IF tableident IS NOT NULL
  THEN
    IF strpos(tableident, '.') > 0
    THEN
      l_nspname := split_part(tableident, '.', 1);
      l_relname := split_part(tableident, '.', 2);
    ELSE
      l_nspname := 'public';
      l_relname := tableident;
    END IF;
    query_text := query_text || 'AND nspname = ' || quote_literal(l_nspname) || ' AND events.relname =  '
                  || quote_literal(l_relname);
  END IF;

  query_text := query_text || ') SELECT nspname, relname, usename, trans_ts, trans_id, trans_sq, eventype' ||
                ', singlerow from subq WHERE TRUE ';

  IF kvlist IS NOT NULL
  THEN
    query_text := query_text || ' AND ( singlerow @> '
                  || quote_literal(json_build_object(split_part(kvlist, ':', 1), split_part(kvlist, ':', 2)))
                  || '::jsonb) ';
  END IF;

  RAISE DEBUG '%', query_text;
  RETURN QUERY EXECUTE query_text || ' order by trans_ts desc,  trans_id asc,  trans_sq asc';

END $body$ LANGUAGE 'plpgsql';

COMMENT ON FUNCTION report_attribute(TEXT, TEXT, DATERANGE) IS $$
Return changes filtered by Column/Value pairs.
Result is a tabel with all changes on rows containing the given pair of
column name and field value.

Arguments:
   attrdetail:    Columns and Value as single string divded by a colon like 'DynProp:TAX_FREE' (case sensitive)
   tablename:     name of table in question or schema and table divided by a dot like 'ed_data.my_table'
   timeframe:     limit query to given range of dates
$$;

